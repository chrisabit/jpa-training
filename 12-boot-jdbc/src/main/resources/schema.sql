CREATE TABLE IF NOT EXISTS TBL_TRAININGS
(
  COL_TITLE VARCHAR(255) NOT NULL,
  COL_LOCATION VARCHAR(255) NOT NULL,
  COL_CREATED_ON TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=INNODB;
