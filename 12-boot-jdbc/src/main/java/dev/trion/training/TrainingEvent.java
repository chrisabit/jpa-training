package dev.trion.training;

import java.time.LocalDate;

public class TrainingEvent {
  private LocalDate eventDate;
  private String location;

  public TrainingEvent() {
  }

  public TrainingEvent(final LocalDate eventDate, final String location) {
    this.eventDate = eventDate;
    this.location = location;
  }

  public LocalDate getEventDate() {
    return eventDate;
  }

  public void setEventDate(final LocalDate eventDate) {
    this.eventDate = eventDate;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(final String location) {
    this.location = location;
  }
}
