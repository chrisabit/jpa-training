package dev.trion.training;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Component
public class SpringJdbcTrainingManager implements TrainingManager {
  private final JdbcTemplate jdbcTemplate;

  public SpringJdbcTrainingManager(final JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public Collection<Training> findTrainings() {
    // tag::query[]
    return jdbcTemplate.query("SELECT COL_TITLE, COL_LOCATION FROM TBL_TRAININGS",
        (rs, rowNum) -> new Training(rs.getString(1), rs.getString(2)));
    // end::query[]
  }

  @Override
  // tag::insert[]
  public void add(final Training training) {
    jdbcTemplate.update(
        "INSERT INTO TBL_TRAININGS (COL_TITLE, COL_LOCATION) VALUES (?, ?)",
        training.getTitle(), training.getLocation());
  }
  // end::insert[]

  @Override
  public Set<Training> findByLocation(final String search) {
    // tag::lambda[]
    var result = new HashSet<Training>();
    result.addAll(
        jdbcTemplate.query(
            "SELECT COL_TITLE, COL_LOCATION FROM TBL_TRAININGS WHERE COL_LOCATION = ? ",
            (rs, i) -> new Training(rs.getString("COL_TITLE"), rs.getString("COL_LOCATION")),
            search));
    return result;
    // end::lambda[]
  }
}
