package dev.trion.training;

public class Training {
  private String title;
  private String location;

  public Training() {
  }

  public Training(final String title, final String location) {
    this.title = title;
    this.location = location;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(final String location) {
    this.location = location;
  }
}
