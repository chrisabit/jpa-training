package dev.trion.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;
import java.util.Set;

@SpringBootApplication
public class TrainingBootApplication {
  public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(TrainingBootApplication.class, args);

    var controller = context.getBean(TrainingController.class);
    // ... controller.trainings() ...
    // end::context[]
    if (args.length < 1) {
      Map<String, Set<Training>> trainings = controller.trainings();
      trainings.entrySet().forEach(TrainingBootApplication::output);
    }
  }

  private static void output(final Map.Entry<String, Set<Training>> l) {
    var out = String.format("%s: %s", l.getKey(),
        l.getValue().stream().map(t -> t.getTitle() + ", ").reduce("", String::concat));
    System.out.println(out);
  }
}
