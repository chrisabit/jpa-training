package dev.trion.training;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

// tag::main[]
@Component
public class TrainingController {
  final Logger logger = LoggerFactory.getLogger(getClass());
  private final TrainingManager manager;

  public TrainingController(final TrainingManager manager) {
    this.manager = manager;
  }
// end::main[]

  public Map<String, Set<Training>> trainings() {
    var trainings = manager.findTrainings()
        .stream().map(Training::getLocation)
        .sorted().distinct()
        .map(l -> new AbstractMap.SimpleEntry<>(l, manager.findByLocation(l)))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    logger.info("Found {} training locations", trainings.size());

    return trainings;
  }
  // tag::main[]
}
// end::main[]
