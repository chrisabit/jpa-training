package write;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import write.entity.Employee;
import write.entity.EmployeeRepository;

@SpringBootApplication
public class WriteJpaSampleApplication {

  public static void main(String[] args) {
    var context = SpringApplication.run(WriteJpaSampleApplication.class, args);
    var repo = context.getBean(EmployeeRepository.class);
    repo.insertEmployee(42L, "Hubert Müller", "Hitmen", 2.23);
    var emp = repo.fetchById(42L);
    emp.ifPresent(System.out::println);
    var delEmp = repo.deleteEmployeeById(42L);
    delEmp.ifPresent(e -> {
      repo.saveEmployee(e);
      System.out.println(e);
    });
  }
}
