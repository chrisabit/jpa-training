package write.entity;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class EmployeeRepository {
  @PersistenceContext
  private EntityManager entityManager;

  @Transactional
  public void insertEmployee(Long id, String name, String department, double salary) {
    entityManager.createNativeQuery(
            "INSERT INTO employee (id, name, department, salary) VALUES (?,?,?,?)")
        .setParameter(1, id)
        .setParameter(2, name)
        .setParameter(3, department)
        .setParameter(4, salary)
        .executeUpdate();
  }

  @Transactional
  public Optional<Employee> deleteEmployeeById(Long id) {
    var emp = fetchById(id);
    emp.ifPresent(e -> {
      entityManager.remove(e);
      entityManager.flush();
      entityManager.clear();
    });
    return emp;
  }

  @Transactional
  public void saveEmployee(Employee emp) {
    var reattachedEmp = entityManager.merge(emp);
    entityManager.persist(reattachedEmp);
  }


  public Optional<Employee> fetchById(Long id) {
    var emp = entityManager.find(Employee.class, id);
    if (emp == null) {
      return Optional.empty();
    }
    else {
      return Optional.of(emp);
    }
  }
}
