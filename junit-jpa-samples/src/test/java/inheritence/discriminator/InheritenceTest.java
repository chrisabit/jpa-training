package inheritence.discriminator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.assertThat;

class InheritenceTest {
  private EntityManagerFactory emf;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-inheritence-disc");
  }

  @Test
  void givenDiscInheritence_whenGenerated_thenPersisted() {
    var em = emf.createEntityManager();
    em.getTransaction().begin();
    var godFather = new GodFather("Sergio Leone", 23);
    em.persist(godFather);
    em.getTransaction().commit();

    var readGodFather = em.find(GodFather.class, 1L);
    assertThat(readGodFather).isNotNull();
    em.close();
  }

  @AfterEach
  void destroy() {
    emf.close();
  }
}