package inheritence.poly;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.assertThat;

class InheritenceTest {
  private EntityManagerFactory emf;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-inheritence-poly");
  }

  @Test
  void givenPolyInheritence_whenGenerated_thenPersisted() {
    var em = emf.createEntityManager();
    em.getTransaction().begin();
    var hitman = new MafiaHitman("Hubert Graete", 42);
    var member = new MafiaMember("Kuno Irgendwer");
    em.persist(hitman);
    em.persist(member);
    em.getTransaction().commit();

    var readMember = em.find(MafiaMember.class, 1L);
    assertThat(readMember).isNotNull();
    var readHitman = (MafiaHitman) readMember;
    assertThat(readHitman.getHitRate()).isEqualTo(42);
    em.close();
  }

  @AfterEach
  void destroy() {
    emf.close();
  }
}