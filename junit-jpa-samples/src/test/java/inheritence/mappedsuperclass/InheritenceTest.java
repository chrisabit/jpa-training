package inheritence.mappedsuperclass;

import inheritence.singletable.GodFather;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.*;

class InheritenceTest {
  private EntityManagerFactory emf;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-inheritence-mapped");
  }

  @Test
  void givenMappedInheritence_whenGenerated_thenPersisted() {
    var em = emf.createEntityManager();
    em.getTransaction().begin();
    var hitman = new MafiaHitman("Hubert Graete", 42);
    em.persist(hitman);
    em.getTransaction().commit();

    var readHitman = em.find(MafiaHitman.class, 1L);
    assertThat(readHitman).isNotNull();
    em.close();
  }

  @AfterEach
  void destroy() {
    emf.close();
  }
}