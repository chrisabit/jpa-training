package relentities.manytomany.entity.noatt;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

class ManyToManyNoAttTest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-many-to-many-noatt");
    em = emf.createEntityManager();
  }

  private List<Training> getTrainingsFromTable() {
    String selectQuery = "SELECT t FROM Training t";
    TypedQuery<Training> query = em.createQuery(selectQuery, Training.class);
    AtomicReference<List<Training>> trainings = new AtomicReference<>(query.getResultList());
    return trainings.get();
  }

  @Test
  public void givenData_whenInsert_thenCreatesManyToMany() {
    var training1 = new Training();
    training1.setName("training1");
    var training2 = new Training();
    training2.setName("training2");

    var student = new Student();
    student.setTrainings(Set.of(training1, training2));

    em.getTransaction().begin();
    em.persist(student);
    em.getTransaction().commit();
    em.clear();

    var readTrainings = getTrainingsFromTable();

    assertThat(readTrainings).isNotNull();
    assertThat(readTrainings).hasSize(2);

    var readTraining = readTrainings.get(0);
    assertThat(readTraining.getStudents()).hasSize(1);
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }
}