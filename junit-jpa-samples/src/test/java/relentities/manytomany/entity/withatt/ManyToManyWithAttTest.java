package relentities.manytomany.entity.withatt;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

class ManyToManyWithAttTest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-many-to-many-withatt");
    em = emf.createEntityManager();
  }

  @Test
  public void givenData_whenInsert_thenCreatesTrainingRates() {
    em.getTransaction().begin();
    var student = new Student();
    em.persist(student);
    var training = new Training();
    em.persist(training);

    var trainingRate = new TrainingRate();
    trainingRate.setId(new TrainingRateKey());
    trainingRate.setStudent(student);
    trainingRate.setTraining(training);
    trainingRate.setRating(42);
    em.persist(trainingRate);
    em.getTransaction().commit();
    em.clear();

    var readTrainingRate = em.find(TrainingRate.class, new TrainingRateKey(student.getId(), training.getId()));

    assertThat(readTrainingRate).isNotNull();
    assertThat(readTrainingRate.getRating()).isEqualTo(42);
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }
}