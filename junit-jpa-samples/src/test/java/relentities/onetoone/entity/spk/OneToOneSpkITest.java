package relentities.onetoone.entity.spk;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

class OneToOneSpkITest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-onetoonespk");
    em = emf.createEntityManager();
  }

  private List<SystemUser> getSystemUsersFromTable() {
    String selectQuery = "SELECT su FROM SystemUser su";
    TypedQuery<SystemUser> query = em.createQuery(selectQuery, SystemUser.class);
    AtomicReference<List<SystemUser>> users = new AtomicReference<>(query.getResultList());
    return users.get();
  }

  @Test
  public void givenData_whenInsert_thenCreatesOneToOne() {
    SystemUser user = new SystemUser();
    user.setUserName("wumpel.putz@trion.de");

    Address address = new Address();
    address.setStreet("Trion-Alee");

    address.setSystemUser(user);
    user.setAddress(address);

    // Wegen Cascade-ALL muss Address nicht persistiert werden!
    em.getTransaction().begin();
    em.persist(user);
    em.getTransaction().commit();
    em.clear();

    var users = getSystemUsersFromTable();

    assertThat(users).isNotNull();
    assertThat(users).hasSize(1);

    var readUser = users.get(0);
    assertThat(user.getUserName()).isEqualTo("wumpel.putz@trion.de");

    var readAddress = readUser.getAddress();
    assertThat(readAddress).isNotNull();
    assertThat(readAddress.getStreet()).isEqualTo("Trion-Alee");
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }
}