package entitypath.api;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.assertThat;

class EntityGraphApiTest {
  private EntityManagerFactory emf;
  private EntityManager em;

  private EntityGraphService underTest;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entitygraph-api");
    em = emf.createEntityManager();
    underTest = new EntityGraphService();
  }

  @Test
  void givenData_whenMessageByFindRead_thenEntityPathLoaded() {
    var message = underTest.fetchEagerByFind(em, 1L);

    assertThat(message).isNotNull();
    assertThat(message.getSubject()).isNotNull();
    assertThat(message.getText()).isNotNull();
    assertThat(message.getPerson()).isNotNull();
    assertThat(message.getLikes()).hasSize(2);
    var readLike = message.getLikes().get(0);
    assertThat(readLike.getPerson()).isNotNull();
  }

  @Test
  void givenData_whenMessageByJpglRead_thenEntityPathLoaded() {
    var message = underTest.fetchEagerByJpql(em, 1L);

    assertThat(message).isNotNull();
    assertThat(message.getSubject()).isNotNull();
    assertThat(message.getText()).isNotNull();
    assertThat(message.getPerson()).isNotNull();
    assertThat(message.getLikes()).hasSize(2);
    var readLike = message.getLikes().get(0);
    assertThat(readLike.getPerson()).isNotNull();
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }
}

