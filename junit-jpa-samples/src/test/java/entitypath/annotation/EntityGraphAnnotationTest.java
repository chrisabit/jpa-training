package entitypath.annotation;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import static org.assertj.core.api.Assertions.assertThat;

class EntityGraphAnnotationTest {
  private EntityManagerFactory emf;
  private EntityManager em;

  private EntityGraphService underTest;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entitygraph-annotation");
    em = emf.createEntityManager();
    underTest = new EntityGraphService();
  }

  @Test
  void givenData_whenPersonsRead_thenComplete() {
    String selectQuery = "SELECT p FROM Person p";
    TypedQuery<Person> query = em.createQuery(selectQuery, Person.class);
    var persons = query.getResultList();

    persons = query.getResultList();
    assertThat(persons).hasSize(3);
  }

  @Test
  void givenData_whenMessageRemovedRead_thenLikesAreCascadedRemoved() {
    var message = em.find(Message.class, 1L);
    em.getTransaction().begin();
    em.remove(message);
    em.getTransaction().commit();

    String selectQuery = "SELECT l FROM Like l";
    TypedQuery<Like> query = em.createQuery(selectQuery, Like.class);
    var likes = query.getResultList();
    likes = query.getResultList();
    assertThat(likes).hasSize(6);
  }


  @Test
  void givenData_whenMessagesRead_thenLazyPersonAndLikesAreRead() {
    String selectQuery = "SELECT m FROM Message m";
    TypedQuery<Message> query = em.createQuery(selectQuery, Message.class);
    var messages = query.getResultList();
    assertThat(messages).hasSize(4);
    assertThat(messages.get(0).getPerson().getName()).isEqualTo("Luischen Sorgenlos");
    assertThat(messages.get(0).getLikes()).hasSize(2);
  }

  @Test
  void givenData_whenMessageByFindRead_thenEntityPathLoaded() {
    var message = underTest.fetchEagerByFind(em, 1L);

    assertThat(message).isNotNull();
    assertThat(message.getSubject()).isNotNull();
    assertThat(message.getText()).isNotNull();
    assertThat(message.getPerson()).isNotNull();
    assertThat(message.getLikes()).hasSize(2);
    var readLike = message.getLikes().get(0);
    assertThat(readLike.getPerson()).isNotNull();
  }

  @Test
  void givenData_whenMessageByJpglRead_thenEntityPathLoaded() {
    var message = underTest.fetchEagerByJpql(em, 1L);

    assertThat(message).isNotNull();
    assertThat(message.getSubject()).isNotNull();
    assertThat(message.getText()).isNotNull();
    assertThat(message.getPerson()).isNotNull();
    assertThat(message.getLikes()).hasSize(2);
    var readLike = message.getLikes().get(0);
    assertThat(readLike.getPerson()).isNotNull();
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }
}

