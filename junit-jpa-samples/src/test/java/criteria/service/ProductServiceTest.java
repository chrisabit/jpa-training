package criteria.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.assertThat;

class ProductServiceTest {
  private EntityManagerFactory emf;
  private EntityManager em;

  private ProductService underTest;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-criteria");
    em = emf.createEntityManager();
    underTest = new ProductService();
  }

  @Test
  void whenThresGreater_thenCorrectProducts() {
    var products = underTest.fetchProductsGreaterThan(em, 42.);
    assertThat(products).hasSize(4);
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }

}