package criteria.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.assertThat;

class EmployeeServiceTest {
  private EntityManagerFactory emf;
  private EntityManager em;

  private EmployeeService underTest;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-criteria");
    em = emf.createEntityManager();
    underTest = new EmployeeService();
  }

  @Test
  void whenNameLike_thenCorrectEmployees() {
    var employees = underTest.fetchByName(em, "Waldo%");
    assertThat(employees).hasSize(1);
  }

  @Test
  void whenOrderedBySalary_thenCorrectEmployees() {
    var employees = underTest.fetchAllOrderedBySalary(em);
    assertThat(employees).hasSize(4);
    assertThat(employees.get(0).getName()).isEqualTo("Holger Kannix");
  }

  @Test
  void whenGroupByDepartment_thenCorrectEmployees() {
    var employeesByDepartment = underTest.fetchCountGroupedByDepartment(em);
    assertThat(employeesByDepartment).hasSize(3);
  }

  @Test
  void whenSummedBySalary_thenCorrectEmployees() {
    var sumSalary = underTest.fetchSumSalary(em);
    assertThat(sumSalary).isEqualTo(1.231495949E7);
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }

}