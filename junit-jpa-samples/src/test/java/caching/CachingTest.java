package caching;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.assertj.core.api.Assertions.*;

class CachingTest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-caching");
    em = emf.createEntityManager();
  }

  @Test
  void givenCachedEntity_whenGenerated_thenPersisted() {
    em.getTransaction().begin();
    var member = new MafiaMember("Hubert Graete");
    em.persist(member);
    em.getTransaction().commit();

    var readMember = em.createQuery("SELECT m FROM MafiaMember m")
        .setHint("org.hibernate.cacheable", true)
        .getResultList();

    assertThat(readMember).isNotNull();
    Cache cache = CacheManager.ALL_CACHE_MANAGERS.get(0).getCache("caching.MafiaMember");
    assertThat(cache.getSize()).isEqualTo(1);
    assertThat(cache.getCacheConfiguration().getMaxEntriesLocalHeap()).isEqualTo(4223);
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }
}