package defentities.entity;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class UniqueColumnITest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-employee");
    em = emf.createEntityManager();
  }

  @Test
  void whenPersistEmployeeWithSameEmail_thenConstraintViolation() {
    Employee emp1 = new Employee();
    emp1.setEmNumber(4711L);
    emp1.setEmail("super.grobi@trion.de");

    Employee emp2 = new Employee();
    emp2.setEmNumber(4712L);
    emp2.setEmail("super.grobi@trion.de");

    em.getTransaction().begin();
    em.persist(emp1);
    em.getTransaction().commit();

    em.getTransaction().begin();
    assertThatThrownBy(() -> {
      em.persist(emp2);
      em.getTransaction().commit();
    }).isInstanceOf(RollbackException.class)
        .getCause()
        .getCause()
        .isInstanceOf(ConstraintViolationException.class);
  }

  @Test
  void whenPersistEmployeeWithSameAddress_thenConstraintViolation() {
    Employee emp1 = new Employee();
    emp1.setEmNumber(4242L);
    emp1.setEmail("hugo.purzel@trion.de");

    Address address1 = new Address();
    address1.setStreetAddress("1234 Musterstrasse");
    emp1.setAddress(address1);

    Employee emp2 = new Employee();
    emp2.setEmNumber(2323L);
    emp2.setEmail("wilhelmine.wumpel@trion.de");
    emp2.setAddress(address1);

    em.getTransaction().begin();
    em.persist(emp1);
    em.persist(address1);
    em.getTransaction().commit();

    em.getTransaction().begin();
    assertThatThrownBy(() -> {
      em.persist(emp2);
      em.getTransaction().commit();
    }).isInstanceOf(RollbackException.class)
        .getCause()
        .getCause()
        .isInstanceOf(ConstraintViolationException.class);
  }
  @AfterEach
  void destroy() {
      em.close();
      emf.close();
  }
}