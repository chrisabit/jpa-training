package defentities.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class SystemUserEntityITest {
    private EntityManagerFactory emf;
    private EntityManager em;

    private void save(SystemUser systemUser) {
        em.getTransaction().begin();
        em.persist(systemUser);
        em.getTransaction().commit();
        em.clear();
    }

    private void delete(SystemUser systemUser) {
        em.getTransaction().begin();
        em.remove(systemUser);
        em.getTransaction().commit();
        em.clear();
    }

    @BeforeEach
    public void setup() {
        emf = Persistence.createEntityManagerFactory("jpa-entity-person");
        em = emf.createEntityManager();
        SystemUser systemUser = new SystemUser();
        systemUser.setFirstName("Wumpel");
        systemUser.setLastName("Putz");
        systemUser.setUserName("wumpelputz");
        save(systemUser);
    }

    @AfterEach
    void destroy() {
        em.close();
        emf.close();
    }

    private SystemUser findByUserName(String userName) {
        String selectQuery = "SELECT u FROM SystemUser u WHERE u.userName = :userName";
        TypedQuery<SystemUser> selectFromUserTypedQuery = em.createQuery(selectQuery, SystemUser.class);
        selectFromUserTypedQuery.setParameter("userName", userName);
        return selectFromUserTypedQuery.getSingleResult();
    }

    @Test
    public void whenNewUserProvided_userIsAdded() {
        SystemUser systemUser = new SystemUser();
        systemUser.setFirstName("John");
        systemUser.setLastName("Doe");
        systemUser.setUserName("jdoe123");
        save(systemUser);
        assertThat(systemUser.getId()).isGreaterThan(0);
    }
    
    @Test
    public void whenUserNameProvided_userIsLoaded() {
        SystemUser user = findByUserName("wumpelputz");
        assertThat(user).isNotNull();
        assertThat("wumpelputz").isEqualTo(user.getUserName());
    }
    
    @Test
    public void whenExistingUserProvided_userIsUpdated() {
        SystemUser user = findByUserName("wumpelputz");
        user.setFirstName("Knallcharge");
        save(user);
        em.detach(user);
        SystemUser updatedUser = findByUserName("wumpelputz");
        assertThat(updatedUser.getFirstName()).isEqualTo("Knallcharge");
    }

    @Test
    public void whenDetachedUserSaves_execIsThrown() {
        SystemUser user = findByUserName("wumpelputz");
        user.setFirstName("Knallcharge");
        em.detach(user);
        assertThatExceptionOfType(PersistenceException.class).isThrownBy(()->save(user));
    }

    @Test
    public void whenExistingUserDeleted_userIsDeleted() {
        SystemUser user = findByUserName("wumpelputz");
        delete(user);
        assertThatExceptionOfType(NoResultException.class).isThrownBy(
            () -> findByUserName("wumpelputz")
        );
    }
    
    @Test
    public void whenExistingUserLoaded_fullNameIsAvailable() {
        SystemUser user = findByUserName("wumpelputz");
        assertThat(user.getFullName()).isEqualTo("Wumpel Putz");
    }
}
