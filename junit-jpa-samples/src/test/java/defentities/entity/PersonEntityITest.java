package defentities.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

public class PersonEntityITest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-person");
    em = emf.createEntityManager();
  }

  @Test
  void persistDetachedPersonThenRetrieveTheDetails() {
    // given
    Person detchPerson = createPersonWithRelevantDetails();
    em.getTransaction().begin();
    em.persist(detchPerson);
    em.getTransaction().commit();
    em.detach(detchPerson);
    var updatedPerson = em.find(Person.class, detchPerson.getId());
    em.getTransaction().begin();
    updatedPerson.setName("Guggelhupf");
    em.persist(updatedPerson);
    em.getTransaction().commit();

    // when
    em.getTransaction().begin();
    var mergedPerson = em.merge(detchPerson);
    em.persist(mergedPerson);
    em.getTransaction().commit();
    var readPerson = em.find(Person.class, 1L);

    // then
    assertThat(readPerson.getId()).isEqualTo(1L);
    assertThat(readPerson.getAge()).isNull();
    assertThat(readPerson.getBirthDate().toString()).isEqualTo("2023-07-20");
    assertThat(readPerson.getName()).isEqualTo("Wumpelputz");
  }

  @Test
  void persistPersonThenRetrieveTheDetails() {
    // given
    Person person = createPersonWithRelevantDetails();
    // when
    em.getTransaction().begin();
    em.persist(person);
    em.getTransaction().commit();
    em.clear();
    List<Person> persons = getPersonsFromTable();
    // then
    assertThat(persons.size()).isEqualTo(1L);
    Person readPerson = persons.get(0);
    assertThat(readPerson.getId()).isEqualTo(1L);
    assertThat(readPerson.getAge()).isNull();
    assertThat(readPerson.getBirthDate().toString()).isEqualTo("2023-07-20");
    assertThat(readPerson.getName()).isEqualTo("Wumpelputz");
  }

  private List<Person> getPersonsFromTable() {
    String selectQuery = "SELECT Person FROM Person Person";
    TypedQuery<Person> selectFromPersonTypedQuery = em.createQuery(selectQuery, Person.class);
    AtomicReference<List<Person>> people = new AtomicReference<>(selectFromPersonTypedQuery.getResultList());
    return people.get();
  }

  private Person createPersonWithRelevantDetails() {
    Person person = new Person();
    person.setAge(20);
    person.setName("Wumpelputz");
    LocalDate localDate = LocalDate.of(2023, 7, 20);
    LocalDate date = localDate.atStartOfDay(ZoneId.systemDefault()).toLocalDate();
    person.setBirthDate(date);
    person.setGender(Gender.MALE);
    return person;
  }

  @AfterEach
  void destroy() {
      em.close();
      emf.close();
  }
}
