package defentities.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

class MafiaITest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-mafia");
    em = emf.createEntityManager();
  }

  private List<Mafia> getMafiasFromTable() {
    String selectQuery = "SELECT m FROM Mafia m";
    TypedQuery<Mafia> query = em.createQuery(selectQuery, Mafia.class);
    AtomicReference<List<Mafia>> mafias = new AtomicReference<>(query.getResultList());
    return mafias.get();
  }

  @Test
  void whenInsertingMafia_thenGodFatherIsMapped() {
    GodFather godFather = new GodFather();
    godFather.setFirstName("Sergio");
    godFather.setLastName("Leone");
    godFather.setEmail("sergio.leone@trion.de");

    Mafia mafia = new Mafia();
    mafia.setName("Palermo-Mafia");
    mafia.setEmail("palermo-mafia@trion.de");
    mafia.setGodFather(godFather);

    em.getTransaction().begin();
    em.persist(mafia);
    em.getTransaction().commit();
    em.clear();

    List<Mafia> mafias = getMafiasFromTable();
    // then
    assertThat(mafias.size()).isEqualTo(1L);
    Mafia readMafia = mafias.get(0);
    assertThat(readMafia.getId()).isEqualTo(1L);
    assertThat(readMafia.getName()).isEqualTo("Palermo-Mafia");
    assertThat(readMafia.getEmail()).isEqualTo("palermo-mafia@trion.de");
    var readGodFather = readMafia.getGodFather();
    assertThat(readGodFather.getFirstName()).isEqualTo("Sergio");
    assertThat(readGodFather.getLastName()).isEqualTo("Leone");
    assertThat(readGodFather.getEmail()).isEqualTo("sergio.leone@trion.de");
    assertThat(readGodFather.getTitle()).isNull();
  }

  @AfterEach
  void destroy() {
      em.close();
      emf.close();
  }
}
