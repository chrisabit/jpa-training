package defentities.entity;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class UniqueConstraintITest {
  private EntityManagerFactory emf;
  private EntityManager em;

  @BeforeEach
  void setup() {
    emf = Persistence.createEntityManagerFactory("jpa-entity-employee");
    em = emf.createEntityManager();
  }

  @Test
  void whenPersistEmployeeWithSameNumberAndStatus_thenConstraintViolation() {
    Employee emp1 = new Employee();
    emp1.setEmNumber(4711L);
    emp1.setActive(Boolean.TRUE);

    Employee emp2 = new Employee();
    emp2.setEmNumber(4711L);
    emp2.setActive(Boolean.TRUE);

    em.getTransaction().begin();
    em.persist(emp1);
    em.getTransaction().commit();

    assertThatThrownBy(() -> {
      em.getTransaction().begin();
      em.persist(emp2);
      em.getTransaction().commit();
    }).isInstanceOf(RollbackException.class)
        .getCause()
        .getCause()
        .isInstanceOf(ConstraintViolationException.class);
  }

  @AfterEach
  void destroy() {
    em.close();
    emf.close();
  }
}