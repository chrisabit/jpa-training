
insert into person (id, name, email) values(1, 'Luischen Sorgenlos', 'lv@trion.de');
insert into person (id, name, email) values(2, 'Antonia Hochstapler', 'ah@trion.de');
insert into person (id, name, email) values(3, 'Olaf Tiefstapler', 'ot@trion.de');

insert into message (id, subject, text, person_id) values(1, 'Alles super!', 'Alles supi im Land!', 1);
insert into message (id, subject, text, person_id) values(2, 'Alles superprima!', 'Alles maximal optimal im Land!', 1);
insert into message (id, subject, text, person_id) values(3, 'Alles schimm!', 'Alles schlimm im Land!', 2);
insert into message (id, subject, text, person_id) values(4, 'Geht so!', 'Alles wie immer im Land!', 3);

insert into like_message (id, type, message_id, person_id) values(1, 'like', 2, 1);
insert into like_message (id, type, message_id, person_id) values(2, 'dislike', 3, 1);
insert into like_message (id, type, message_id, person_id) values(3, 'dislike', 4, 1);
insert into like_message (id, type, message_id, person_id) values(4, 'like', 1, 2);
insert into like_message (id, type, message_id, person_id) values(5, 'dislike', 3, 2);
insert into like_message (id, type, message_id, person_id) values(6, 'like', 1, 3);
insert into like_message (id, type, message_id, person_id) values(7, 'like', 2, 3);
insert into like_message (id, type, message_id, person_id) values(8, 'dislike', 3, 3);