  
insert into product (id, name, detail, price) values(1, 'Fleischwursthammer', 'Fleischthekenfreude', 42.23);

insert into product (id, name, detail, price) values(2, 'Knotenmacher', 'Macher von Knoten', 666.66);

insert into product (id, name, detail, price) values(3, 'Tampolin', 'Paerpocks Freude', 34.22);

insert into product (id, name, detail, price) values(4, 'Gasleitung', 'Was zum Sprengen', 1321123.72);

insert into product (id, name, detail, price) values(5, 'Solaranlage', 'Wenig Energie', 4222.23);

insert into product (id, name, detail, price) values(6, 'Kohle', 'Viel Energie', 32.23);

insert into employee (id, name, department, salary) values(1, 'Waldo Wutz', 'Hitmen', 1232.23);

insert into employee (id, name, department, salary) values(2, 'Kuno Knallmann', 'Hitmen', 1242.42);

insert into employee (id, name, department, salary) values(3, 'Olga Komrufufs', 'HR', 242.42);

insert into employee (id, name, department, salary) values(4, 'Holger Kannix', 'Chefleiste', 12312242.42);
