package criteria.service;

import criteria.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class EmployeeService {
  public List<Employee> fetchByName(EntityManager em, String likeName) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Employee> cr = cb.createQuery(Employee.class);
    Root<Employee> root = cr.from(Employee.class);
    List<Predicate> predicates = List.of(cb.like(root.get("name"), likeName), cb.isNotNull(root.get("department")));
    cr.select(root).where(predicates.toArray(Predicate[]::new));
    TypedQuery<Employee> query = em.createQuery(cr);
    return query.getResultList();
  }

  public List<Employee> fetchAllOrderedBySalary(EntityManager em) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Employee> cr = cb.createQuery(Employee.class);
    Root<Employee> root = cr.from(Employee.class);
    cr.select(root).where(cb.isNotNull(root.get("name")));
    cr.orderBy(cb.desc(root.get("salary")));
    TypedQuery<Employee> query = em.createQuery(cr);
    return query.getResultList();
  }

  public Double fetchSumSalary(EntityManager em) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Double> cr = cb.createQuery(Double.class);
    Root<Employee> root = cr.from(Employee.class);
    cr.select(cb.sum(root.get("salary")));
    TypedQuery<Double> query = em.createQuery(cr);
    return query.getSingleResult();
  }

  public List<Object[]> fetchCountGroupedByDepartment(EntityManager em) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Object[]> cr = cb.createQuery(Object[].class);
    Root<Employee> root = cr.from(Employee.class);
    cr.groupBy(root.get("department"));
    cr.multiselect(root.get("department"), cb.count(root));
    TypedQuery<Object[]> query = em.createQuery(cr);
    return query.getResultList();
  }
}
