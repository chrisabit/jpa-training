package criteria.service;

import criteria.entity.Product;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

public class ProductService {
  public List<Product> fetchProductsGreaterThan(EntityManager em, double thresholdPrice) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Product> cr = cb.createQuery(Product.class);
    Root<Product> root = cr.from(Product.class);
    cr.select(root).where(cb.gt(root.get("price"), thresholdPrice));
    // cr.add(Restrictions.gt("itemPrice", 1000));
    TypedQuery<Product> query = em.createQuery(cr);
    return query.getResultList();
  }

  public void updateProduct(EntityManager em, String searchName, double price) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaUpdate<Product> criteriaUpdate =
        cb.createCriteriaUpdate(Product.class);
    Root<Product> root =
        criteriaUpdate.from(Product.class);
    criteriaUpdate.set("price", price);
    criteriaUpdate.where(
        cb.equal(root.get("name"), searchName));
    em.getTransaction().begin();
    em.createQuery(criteriaUpdate).executeUpdate();
    em.getTransaction().commit();
  }
}
