package criteria.entity;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {
  @Id
  @GeneratedValue
  @Column(name = "id")
  private Long id;
  private String name;
  private String detail;
  private Double price;

  public Product() {
  }

  public Product(String name, String detail, Double price) {
    this.name = name;
    this.detail = detail;
    this.price = price;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }
}
