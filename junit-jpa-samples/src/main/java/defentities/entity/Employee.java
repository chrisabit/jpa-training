package defentities.entity;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNumberAndStatus", columnNames = { "emNumber", "active" }) })
public class Employee {
  @Id
  @GeneratedValue
  private Long id;
  private String username;
  private String password;
  @Column(unique=true)
  private String email;
  private Long emNumber;
  private Boolean active;
  private String securityNumber;
  private String department;

  @OneToOne
  @JoinColumn(name = "address_id", referencedColumnName = "id", unique = true)
  private Address address;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Long getEmNumber() {
    return emNumber;
  }

  public void setEmNumber(Long emNumber) {
    this.emNumber = emNumber;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getSecurityNumber() {
    return securityNumber;
  }

  public void setSecurityNumber(String securityNumber) {
    this.securityNumber = securityNumber;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }
}