package defentities.entity;

import javax.persistence.*;

@Entity
public class Mafia {
  @Id
  @GeneratedValue
  private Integer id;
  private String name;
  private String email;
  @Embedded
  @AttributeOverrides({
      @AttributeOverride( name = "firstName", column = @Column(name = "gf_first_name")),
      @AttributeOverride( name = "lastName", column = @Column(name = "gf_last_name")),
      @AttributeOverride( name = "title", column = @Column(name = "gf_title")),
      @AttributeOverride( name = "email", column = @Column(name = "gf_email"))
  })
  private GodFather godFather;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public GodFather getGodFather() {
    return godFather;
  }

  public void setGodFather(GodFather godFather) {
    this.godFather = godFather;
  }
}
