package defentities.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class GodFather {
  @Column(name = "gf_first")
  private String firstName;
  @Column(name = "gf_last")
  private String lastName;
  @Column(name = "gf_title")
  private String title;
  @Column(name = "gf_email")
  private String email;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
