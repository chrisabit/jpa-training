package defentities.entity;

import javax.persistence.*;

@Entity
public class Address {
  @Id
  @GeneratedValue
  private Long id;

  private String streetAddress;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }
}
