package defentities.entity;

import javax.persistence.*;

@Entity
@EntityListeners(AuditTrailListener.class)
@Table(name="CUSTOM_USER")
public class SystemUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "USER_NAME", columnDefinition="varchar(255) default 'Wumpel Putz'")
    private String userName;
    @Column(name = "FIRST_NAME", length = 50, nullable = false)
    private String firstName;
    @Column(name = "LAST_NAME", length = 50, nullable = false)
    private String lastName;
    @Transient
    private String fullName;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getFullName() {
        return fullName;
    }
    
    @PrePersist
    public void logNewUserAttempt() {
        System.out.println("Attempting to add new user with username: " + userName);
    }
    
    @PostPersist
    public void logNewUserAdded() {
        System.out.println("Added user '" + userName + "' with ID: " + id);
    }
    
    @PreRemove
    public void logUserRemovalAttempt() {
        System.out.println("Attempting to delete user: " + userName);
    }
    
    @PostRemove
    public void logUserRemoval() {
        System.out.println("Deleted user: " + userName);
    }
    
    @PreUpdate
    public void logUserUpdateAttempt() {
        System.out.println("Attempting to update user: " + userName);
    }
    
    @PostUpdate
    public void logUserUpdate() {
        System.out.println("Updated user: " + userName);
    }
    
    @PostLoad
    public void logUserLoad() {
        fullName = firstName + " " + lastName;
    }
}
