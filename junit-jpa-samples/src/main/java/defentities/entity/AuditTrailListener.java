package defentities.entity;

import javax.persistence.*;

public class AuditTrailListener {
    @PrePersist
    @PreUpdate
    @PreRemove
    private void beforeAnyUpdate(SystemUser systemUser) {
        if (systemUser.getId() == null) {
            System.out.println("[USER AUDIT] About to add a user");
        } else {
            System.out.println("[USER AUDIT] About to update/delete user: " + systemUser.getId());
        }
    }
    
    @PostPersist
    @PostUpdate
    @PostRemove
    private void afterAnyUpdate(SystemUser systemUser) {
        System.out.println("[USER AUDIT] add/update/delete complete for user: " + systemUser.getId());
    }
    
    @PostLoad
    private void afterLoad(SystemUser systemUser) {
        System.out.println("[USER AUDIT] user loaded from database: " + systemUser.getId());
    }
}
