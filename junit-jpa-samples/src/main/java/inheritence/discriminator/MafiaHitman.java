package inheritence.discriminator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Hit")
public class MafiaHitman extends MafiaMember {
  private int hitRate;

  public MafiaHitman() {
  }

  public MafiaHitman(String name, int hitRate) {
    super(name);
    this.hitRate = hitRate;
  }
}
