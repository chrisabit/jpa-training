package inheritence.discriminator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("God")
public class GodFather extends MafiaMember {
  private int rank;

  public GodFather() {
  }

  public GodFather(String name, int rank) {
    super(name);
    this.rank = rank;
  }
}
