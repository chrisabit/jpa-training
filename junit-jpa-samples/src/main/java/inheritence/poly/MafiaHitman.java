package inheritence.poly;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class MafiaHitman extends MafiaMember {
  private int hitRate;

  public MafiaHitman() {
  }

  public MafiaHitman(String name, int hitRate) {
    super(name);
    this.hitRate = hitRate;
  }

  public int getHitRate() {
    return hitRate;
  }

  public void setHitRate(int hitRate) {
    this.hitRate = hitRate;
  }
}
