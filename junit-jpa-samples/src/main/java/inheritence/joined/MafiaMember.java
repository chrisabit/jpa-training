package inheritence.joined;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class MafiaMember {
  @Id
  @GeneratedValue
  private Long id;
  private String name;

  public MafiaMember() {
  }

  public MafiaMember(String name) {
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
