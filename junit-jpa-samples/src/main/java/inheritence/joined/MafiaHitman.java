package inheritence.joined;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "hitman_id")
public class MafiaHitman extends MafiaMember {
  private int hitRate;

  public MafiaHitman() {
  }

  public MafiaHitman(String name, int hitRate) {
    super(name);
    this.hitRate = hitRate;
  }
}
