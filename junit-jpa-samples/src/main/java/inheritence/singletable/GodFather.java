package inheritence.singletable;

import javax.persistence.Entity;

@Entity // Entity!
public class GodFather extends MafiaMember {
  private int rank;

  public GodFather() {
  }

  public GodFather(String name, int rank) {
    super(name);
    this.rank = rank;
  }
}
