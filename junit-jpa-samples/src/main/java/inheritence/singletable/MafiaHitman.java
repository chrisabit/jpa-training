package inheritence.singletable;

import javax.persistence.Entity;

@Entity // Entity!
public class MafiaHitman extends MafiaMember {
  private int hitRate;

  public MafiaHitman() {
  }

  public MafiaHitman(String name, int hitRate) {
    super(name);
    this.hitRate = hitRate;
  }
}
