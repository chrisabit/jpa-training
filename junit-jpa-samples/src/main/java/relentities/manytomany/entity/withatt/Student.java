package relentities.manytomany.entity.withatt;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Student {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "student_has_trainings",
        joinColumns = @JoinColumn(name = "student_id"),
        inverseJoinColumns = @JoinColumn(name = "training_id"))
    private Set<Training> trainings;

    @OneToMany(mappedBy = "student")
    private Set<TrainingRate> trainingRates;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Training> getTrainings() {
        return trainings;
    }

    public void setTrainings(Set<Training> trainings) {
        this.trainings = trainings;
    }

    public Set<TrainingRate> getTrainingRates() {
        return trainingRates;
    }

    public void setTrainingRates(Set<TrainingRate> trainingRates) {
        this.trainingRates = trainingRates;
    }
}
