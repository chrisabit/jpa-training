package relentities.manytomany.entity.withatt;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TrainingRateKey implements Serializable {
    @Column(name = "student_id")
    private Long studentId;
    @Column(name = "training_id")
    private Long trainingId;

    public TrainingRateKey() {
    }

    public TrainingRateKey(Long studentId, Long trainingId) {
        this.studentId = studentId;
        this.trainingId = trainingId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(Long trainingId) {
        this.trainingId = trainingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrainingRateKey that = (TrainingRateKey) o;

        if (!Objects.equals(studentId, that.studentId)) return false;
        return Objects.equals(trainingId, that.trainingId);
    }

    @Override
    public int hashCode() {
        int result = studentId != null ? studentId.hashCode() : 0;
        result = 31 * result + (trainingId != null ? trainingId.hashCode() : 0);
        return result;
    }
}
