package relentities.manytomany.entity.withatt;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Training {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "trainings")
    private Set<Student> students = new HashSet<>();

    @OneToMany(mappedBy = "training")
    private Set<TrainingRate> trainingRates;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<TrainingRate> getTrainingRates() {
        return trainingRates;
    }

    public void setTrainingRates(Set<TrainingRate> trainingRates) {
        this.trainingRates = trainingRates;
    }
}
