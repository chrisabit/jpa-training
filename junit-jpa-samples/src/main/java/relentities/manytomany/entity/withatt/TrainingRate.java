package relentities.manytomany.entity.withatt;

import javax.persistence.*;

@Entity
public class TrainingRate {
  @EmbeddedId
  private TrainingRateKey id;

  @ManyToOne
  @MapsId("studentId")
  @JoinColumn(name = "student_id")
  Student student;

  @ManyToOne
  @MapsId("trainingId")
  @JoinColumn(name = "training_id")
  Training training;

  private int rating;

  public TrainingRateKey getId() {
    return id;
  }

  public void setId(TrainingRateKey id) {
    this.id = id;
  }

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }

  public Training getTraining() {
    return training;
  }

  public void setTraining(Training training) {
    this.training = training;
  }

  public int getRating() {
    return rating;
  }

  public void setRating(int rating) {
    this.rating = rating;
  }
}
