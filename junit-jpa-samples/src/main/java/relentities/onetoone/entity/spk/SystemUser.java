package relentities.onetoone.entity.spk;


import javax.persistence.*;

@Entity
public class SystemUser {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "username")
    private String userName;

    @OneToOne(mappedBy = "systemUser", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Address address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
