package relentities.onetoone.entity.spk;

import javax.persistence.*;

@Entity
public class Address {

    @Id
    @Column(name = "user_id")
    private Long id;

    @Column(name = "street")
    private String street;

    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    private SystemUser systemUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public SystemUser getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }
}
