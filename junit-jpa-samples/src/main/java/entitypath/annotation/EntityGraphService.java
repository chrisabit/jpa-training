package entitypath.annotation;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;

public class EntityGraphService {
  public Message fetchEagerByFind(EntityManager em, Long id) {
    EntityGraph entityGraph = em.getEntityGraph("entity-graph-with-likes-subgraph");
    Map<String, Object> properties = new HashMap<>();
    properties.put("javax.persistence.fetchgraph", entityGraph);
    return em.find(Message.class, id, properties);
  }

  public Message fetchEagerByJpql(EntityManager em, Long id) {
    EntityGraph entityGraph = em.getEntityGraph("entity-graph-with-likes-subgraph");
    return em.createQuery("SELECT m FROM Message m WHERE m.id = :id", Message.class)
        .setParameter("id", id)
        .setHint("javax.persistence.fetchgraph", entityGraph)
        .getSingleResult();
  }
}
