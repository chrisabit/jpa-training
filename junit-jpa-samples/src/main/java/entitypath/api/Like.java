package entitypath.api;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Table(name="like_message")
public class Like {
  @Id
  @GeneratedValue
  private Long id;

  private String type;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="message_id")
  private Message message;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="person_id")
  private Person person;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Message getMessage() {
    return message;
  }

  public void setMessage(Message message) {
    this.message = message;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }
}