package entitypath.api;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;

public class EntityGraphService {
  private EntityGraph createEntityGraph(EntityManager em) {
    EntityGraph<Message> entityGraph = em.createEntityGraph(Message.class);
    entityGraph.addAttributeNodes("subject");
    entityGraph.addAttributeNodes("person");
    entityGraph.addSubgraph("likes")
        .addAttributeNodes("person");
    return entityGraph;
  }

  public Message fetchEagerByFind(EntityManager em, Long id) {
    EntityGraph entityGraph = createEntityGraph(em);
    Map<String, Object> properties = new HashMap<>();
    properties.put("javax.persistence.fetchgraph", entityGraph);
    return em.find(Message.class, id, properties);
  }

  public Message fetchEagerByJpql(EntityManager em, Long id) {
    EntityGraph entityGraph = createEntityGraph(em);
    return em.createQuery("SELECT m FROM Message m WHERE m.id = :id", Message.class)
        .setParameter("id", id)
        .setHint("javax.persistence.fetchgraph", entityGraph)
        .getSingleResult();
  }

  public Message fetchEager(EntityManager em) {
    EntityGraph<Message> entityGraph = em.createEntityGraph(Message.class);
    entityGraph.addAttributeNodes("subject");
    entityGraph.addAttributeNodes("person");
    entityGraph.addSubgraph("likes")
        .addAttributeNodes("person");
    return null;
  }

}
