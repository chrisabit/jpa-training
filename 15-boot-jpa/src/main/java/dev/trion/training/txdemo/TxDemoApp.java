package dev.trion.training.txdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Optional;

//@SpringBootApplication
public class TxDemoApp {
  public static void main(String[] args) {
    System.setProperty("spring.jpa.hibernate.ddl-auto", "create-drop");
    System.setProperty("spring.jpa.show-sql", "true");
    System.setProperty("spring.jpa.open-in-view", "true");
    System.setProperty("logging.level.org.springframework.transaction", "trace");

    // SpringApplication.run(TxDemoApp.class, args);
  }

  @Bean
  ApplicationRunner applicationRunner(UserService userService) {
    return args -> {
      User user = userService.create("Thomas");
      userService.readonly(user.getId(), "Oliver");
      userService.regular(user.getId(), "Susi");
      userService.print(user.getId());
    };
  }
}

//@Service
class UserService {
  @Autowired
  private UserRepository userRepository;

  @Transactional
  public User create(String name) {
    System.out.println("create");
    return userRepository.save(new User(name));
  }

  @Transactional(readOnly = true)
  public void readonly(Long id, String name) {
    System.out.println("readonly");
    Optional<User> user = userRepository.findById(id);
    user.ifPresent(u -> u.setName(name));
  }

  @Transactional
  public void regular(Long id, String name) {
    System.out.println("regular");
    Optional<User> user = userRepository.findById(id);
    user.ifPresent(u -> u.setName(name));
  }

  public void print(Long id) {
    userRepository.findById(id).ifPresent(System.out::println);
  }

  public Iterable<User> findAll() {
    return userRepository.findAll();
  }
}

//@RestController
class Ctrl {
  //@Autowired
  private UserRepository userRepository;

  @GetMapping("/try")
  public Iterable<User> user() {
    userRepository.findById(1L).ifPresent(u -> u.setName("magic"));
    return userRepository.findAll();
  }

  @GetMapping("/db")
  public Iterable<User> db() {
    return userRepository.findAll();
  }
}

interface UserRepository extends CrudRepository<User, Long> {
}

@Entity
class User {
  @Id
  @GeneratedValue
  private Long id;
  private String name;

  public User(String name) {
    this.name = name;
  }

  public User() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
