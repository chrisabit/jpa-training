package dev.trion.training;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "TBL_TRAININGS")
public class Training {
  //    @JsonIgnore
  @Id
  @Column(name = "COL_ID", nullable = false)
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  @Column(name = "COL_TITLE")
  private String title;

  @Column(name = "COL_LOCATION")
  private String location;

  public Training() {
  }

  public Training(final String title, final String location) {
    this.title = title;
    this.location = location;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(final String location) {
    this.location = location;
  }
}
