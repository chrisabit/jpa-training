package dev.trion.training;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

// tag::main[]
@RestController
public class TrainingController {
  final Logger logger = LoggerFactory.getLogger(getClass());
  private final TrainingManager manager;

  public TrainingController(final TrainingManager manager) {
    this.manager = manager;
  }

  @GetMapping("/trainings")
  public Map<String, Set<Training>> trainings() {
    var trainings = manager.findTrainings()
        .stream().map(Training::getLocation)
        .sorted().distinct()
        .map(l -> new AbstractMap.SimpleEntry<>(l, manager.findByLocation(l)))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    logger.info("Found {} training locations", trainings.size());

    return trainings;
  }
  // tag::main[]

  @Autowired
  private TrainingRepository trainingRepository;

  @Transactional
  @GetMapping("/magic")
  public List<Training> magic() {
    trainingRepository.findById("1").ifPresent(t -> t.setTitle("magic"));
    return trainingRepository.findAll();
  }
}
// end::main[]
