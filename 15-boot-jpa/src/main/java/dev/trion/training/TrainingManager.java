package dev.trion.training;

import java.util.Collection;
import java.util.Set;

// tag::main[]
public interface TrainingManager {
  Collection<Training> findTrainings();

  void add(Training training);

  Set<Training> findByLocation(String location);
}
// end::main[]
