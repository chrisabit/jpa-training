package dev.trion.training;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface TrainingRepository extends JpaRepository<Training, String> {
  Set<Training> findByLocation(String location);
}
