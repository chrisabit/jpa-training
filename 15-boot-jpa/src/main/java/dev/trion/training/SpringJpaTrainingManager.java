package dev.trion.training;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
@Primary
public class SpringJpaTrainingManager implements TrainingManager {
  private final TrainingRepository trainingRepository;

  public SpringJpaTrainingManager(TrainingRepository trainingRepository) {
    this.trainingRepository = trainingRepository;
  }

  @Override
  public Collection<Training> findTrainings() {
    return trainingRepository.findAll();
  }

  @Override
  public void add(Training training) {
    trainingRepository.save(training);
  }

  @Override
  public Set<Training> findByLocation(String location) {
    return trainingRepository.findByLocation(location);
  }
}
