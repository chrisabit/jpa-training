package dev.trion.training;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("dev")
@SpringBootTest
class TrainingBootApplicationTests {
  @Test
  void contextLoads() {
    System.out.println("kontext geladen");
  }
}
