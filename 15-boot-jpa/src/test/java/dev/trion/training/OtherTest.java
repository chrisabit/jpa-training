package dev.trion.training;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@DirtiesContext
@SpringBootTest
public class OtherTest {
  @Test
  void contextLoads() {
    System.out.println("other kontext geladen");
  }

  @Test
  void contextLoadsAgain() {
    System.out.println("again other kontext geladen");
  }

}
