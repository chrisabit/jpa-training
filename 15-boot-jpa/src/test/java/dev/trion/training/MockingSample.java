package dev.trion.training;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {MockingSample.class, MockingSample.B.class, MockingSample.A.class, MockingSample.MockInit.class})
public class MockingSample {
  @Autowired
  private A a;

  //too late!
  //@MockBean
  //private static B b;
  //@BeforeEach
  //void setUp()
  //{
  //    when(b.methodToBeMocked()).thenReturn("mocked!");
  //}

  @TestConfiguration
  static class MockInit {
    @Primary
    @Bean
    B makeB() {
      var b = mock(B.class);
      when(b.methodToBeMocked()).thenReturn("mocked!");
      return b;
    }
  }


  @Test
  void mytest() {
    a.anyMethod();
  }

  @Service
  public static class A {
    @Autowired
    private B b;

    private String result;

    @PostConstruct
    public void postconstruct() {
      result = b.methodToBeMocked();
      System.out.println("postconstruct: " + result);
    }

    public void anyMethod() {
      System.out.println("anymethod: " + result);
    }
  }

  @Service
  public static class B {
    public String methodToBeMocked() {
      return "real";
    }
  }

}
