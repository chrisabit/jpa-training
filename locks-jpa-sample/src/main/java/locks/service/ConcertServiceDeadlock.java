package locks.service;

import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import locks.entity.Concert;
import locks.entity.ConcertRepository;
import locks.entity.TicketRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

//@Service
public class ConcertServiceDeadlock implements ConcertService {
  private final ConcertRepository concertRepository;
  private final TicketRepository ticketRepository;

  @PostConstruct
  @Transactional
  public void init() {
    concertRepository.save(new Concert(1L, "42-23", LocalDateTime.now(), 2));
    concertRepository.save(new Concert(2L, "23-42", LocalDateTime.now(), 2));
  }

  public ConcertServiceDeadlock(ConcertRepository concertRepository, TicketRepository ticketRepository) {
    this.concertRepository = concertRepository;
    this.ticketRepository = ticketRepository;
  }


  private void fetchAndChangeConcertCapacity(Long concertId) throws Exception {
    var concert = concertRepository.findWithLockingById(concertId).get();
    concert.setCapacity(concert.getCapacity() + 1);
    Thread.sleep(1_000);
  }

  @Transactional
  public void changeConcert1() throws Exception {
    fetchAndChangeConcertCapacity(1L);
    fetchAndChangeConcertCapacity(2L);
    Thread.sleep(1_000);
  }

  @Transactional
  public void changeConcert2() throws Exception {
    fetchAndChangeConcertCapacity(2L);
    fetchAndChangeConcertCapacity(1L);
    Thread.sleep(1_000);
  }

  @Override
  public void showStatus() {

  }
}
