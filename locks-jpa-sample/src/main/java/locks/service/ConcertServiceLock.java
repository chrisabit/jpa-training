package locks.service;

import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import locks.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ConcertServiceLock implements ConcertService {
  private Logger logger = LoggerFactory.getLogger(ConcertServiceLock.class);
  private final ConcertRepository concertRepository;
  private final TicketRepository ticketRepository;
  private final CustomRepository customRepository;

  @PostConstruct
  @Transactional
  public void init() {
    concertRepository.save(new Concert(1L, "42-23", LocalDateTime.now(), 1));
  }

  public ConcertServiceLock(ConcertRepository concertRepository,
                            TicketRepository ticketRepository,
                            CustomRepository customRepository) {
    this.concertRepository = concertRepository;
    this.ticketRepository = ticketRepository;
    this.customRepository = customRepository;
  }


  private void saveNewTicket(String firstName, String lastName, Concert concert) {
    if (concert.getCapacity() <= concert.getTickets().size()) {
      throw new CapacityExceededException();
    }
    var ticket = new Ticket();
    ticket.setFirstName(firstName);
    ticket.setLastName(lastName);
    concert.addTicket(ticket);
    ticketRepository.save(ticket);
  }

  @Override
  @Transactional
  public void changeConcert1() throws Exception {
    var concert = concertRepository.findWithLockingById(1L).get();
    saveNewTicket("Hubert", "Kachelmann", concert);
    Thread.sleep(1_000);
  }

  @Override
  @Transactional
  public void changeConcert2() throws Exception {
    var concert = concertRepository.findWithLockingById(1L).get();
    saveNewTicket("Petra", "Müller", concert);
    Thread.sleep(1_000);
  }

  @Override
  public void showStatus() {
    logger.info("#Concerts=" + customRepository.findConcerts().size());
    logger.info("#Tickets=" + customRepository.findTickets().size());
  }
}
