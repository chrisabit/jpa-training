package locks.service;

public interface ConcertService {
    void init();

    void changeConcert1() throws Exception;

    void changeConcert2() throws Exception;

    void showStatus();
}
