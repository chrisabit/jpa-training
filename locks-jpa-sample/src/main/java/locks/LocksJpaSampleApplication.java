package locks;

import jakarta.annotation.Resource;
import locks.service.ConcertService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.apache.commons.lang3.function.FailableRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
public class LocksJpaSampleApplication implements CommandLineRunner {

  @Resource
  private ConcertService concertService;

  public static void main(String[] args) {
    SpringApplication.run(LocksJpaSampleApplication.class, args);
  }

  @Override
  public void run(String... args) {
    ExecutorService executor = Executors.newFixedThreadPool(2);
    executor.execute(safeRunnable(concertService::changeConcert1));
    executor.execute(safeRunnable(concertService::changeConcert2));
    executor.shutdown();
  }

  private Runnable safeRunnable(FailableRunnable<Exception> runnable) {
    return () -> {
      try {
        runnable.run();
      } catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        concertService.showStatus();
      }
    };
  }
}