package locks.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Version;

import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class Concert {
  @Id
  private Long id;
  private String number;
  private LocalDateTime time;
  private Integer capacity;

  @OneToMany(mappedBy = "concert")
  private Set<Ticket> tickets;

  public Concert() {
  }

  public Concert(Long id, String number, LocalDateTime time, Integer capacity) {
    this.id = id;
    this.number = number;
    this.time = time;
    this.capacity = capacity;
  }

  @Version
  private Long version;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public LocalDateTime getTime() {
    return time;
  }

  public void setTime(LocalDateTime time) {
    this.time = time;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public Set<Ticket> getTickets() {
    return tickets;
  }

  public void setTickets(Set<Ticket> tickets) {
    this.tickets = tickets;
  }

  public void addTicket(Ticket ticket) {
    ticket.setConcert(this);
    getTickets().add(ticket);
  }
}