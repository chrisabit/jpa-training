package locks.entity;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public CustomRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Concert> findConcerts() {
        return entityManager.createQuery("SELECT c FROM Concert c", Concert.class).getResultList();
    }
    public List<Ticket> findTickets() {
        return entityManager.createQuery("SELECT t FROM Ticket t", Ticket.class).getResultList();
    }
}
