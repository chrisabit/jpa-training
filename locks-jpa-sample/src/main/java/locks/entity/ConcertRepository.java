package locks.entity;

import jakarta.persistence.LockModeType;
import jakarta.persistence.QueryHint;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConcertRepository
    extends CrudRepository<Concert, Long> {
  @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
  //@Lock(LockModeType.PESSIMISTIC_WRITE)
  @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="10000")})
  Optional<Concert> findWithLockingById(Long id);
}
